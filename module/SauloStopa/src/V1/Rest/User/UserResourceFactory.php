<?php
namespace SauloStopa\V1\Rest\User;

class UserResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('SauloStopa\V1\Rest\User\UserMapper');
        return new UserResource($mapper);
    }
}
