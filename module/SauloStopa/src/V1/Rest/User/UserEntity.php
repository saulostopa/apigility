<?php
namespace SauloStopa\V1\Rest\User;

class UserEntity
{
    public $id;
    public $email;
    public $username;

    public function getArrayCopy()
    {
        return array(
            'id'        => $this->id,
            'email'     => $this->email,
            'username'  => $this->username
        );
    }

    public function exchangeArray(array $array)
    {
        $this->id       = $array['id'];
        $this->email    = $array['email'];
        $this->username = $array['username'];
    }
}
