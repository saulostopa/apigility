<?php
return [
    'service_manager' => [
        'factories' => [
            \SauloStopa\V1\Rest\User\UserResource::class => \SauloStopa\V1\Rest\User\UserResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'saulo-stopa.rest.user' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/user[/:user_id]',
                    'defaults' => [
                        'controller' => 'SauloStopa\\V1\\Rest\\User\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'saulo-stopa.rest.user',
        ],
    ],
    'zf-rest' => [
        'SauloStopa\\V1\\Rest\\User\\Controller' => [
            'listener' => \SauloStopa\V1\Rest\User\UserResource::class,
            'route_name' => 'saulo-stopa.rest.user',
            'route_identifier_name' => 'user_id',
            'collection_name' => 'user',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => '10',
            'page_size_param' => 'limit',
            'entity_class' => \SauloStopa\V1\Rest\User\UserEntity::class,
            'collection_class' => \SauloStopa\V1\Rest\User\UserCollection::class,
            'service_name' => 'User',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'SauloStopa\\V1\\Rest\\User\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'SauloStopa\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.saulo-stopa.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'SauloStopa\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.saulo-stopa.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \SauloStopa\V1\Rest\User\UserEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'saulo-stopa.rest.user',
                'route_identifier_name' => 'user_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \SauloStopa\V1\Rest\User\UserCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'saulo-stopa.rest.user',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'SauloStopa\\V1\\Rest\\User\\Controller' => [
            'input_filter' => 'SauloStopa\\V1\\Rest\\User\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'SauloStopa\\V1\\Rest\\User\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'username',
                'field_type' => 'string',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'email',
                'field_type' => 'string',
            ],
        ],
    ],
];
