<?php
return [
    'SauloStopa\\V1\\Rest\\User\\Controller' => [
        'description' => 'List all users',
        'collection' => [
            'description' => 'List all users from table',
            'GET' => [
                'description' => 'List all users from table users',
                'response' => '{
   "_links": {
       "self": {
           "href": "/user"
       },
       "first": {
           "href": "/user?page={page}"
       },
       "prev": {
           "href": "/user?page={page}"
       },
       "next": {
           "href": "/user?page={page}"
       },
       "last": {
           "href": "/user?page={page}"
       }
   }
   "_embedded": {
       "user": [
           {
               "_links": {
                   "self": {
                       "href": "/user[/:user_id]"
                   }
               }
              "id": "",
              "username": "",
              "email": ""
           }
       ]
   }
}',
            ],
        ],
    ],
];
