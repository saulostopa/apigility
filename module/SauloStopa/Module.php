<?php
namespace SauloStopa;

use Zend\Db\TableGateway\TableGateway;
use ZF\Apigility\Provider\ApigilityProviderInterface;
use SauloStopa\V1\Rest\User\UserEntity;
use SauloStopa\V1\Rest\User\UserMapper;
use Zend\Db\ResultSet\ResultSet;


class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'UserTableGateway' => function($ServiceManager) {
                    $dbAdapter = $ServiceManager->get('CiaWebTreino');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserEntity());
                    return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
                },
                'SauloStopa\V1\Rest\User\UserMapper' => function($ServiceManager) {
                    $tableGateway = $ServiceManager->get('UserTableGateway');
                    return new UserMapper($tableGateway);
                }
            )
        );
    }

    public function getAutoloaderConfig()
    {
        return [
            'ZF\Apigility\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src',
                ],
            ],
        ];
    }
}
