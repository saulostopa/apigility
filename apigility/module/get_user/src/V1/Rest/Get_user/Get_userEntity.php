<?php
namespace get_user\V1\Rest\Get_user;

class Get_userEntity
{
    public $id;
    public $name;
    public $email;

    public function getArrayCopy()
    {
        return array(
             'id'       => $this->id
            ,'name'     => $this->name
            ,'email'    => $this->email
        );
    }

    public function exchangeArray(array $array)
    {
        $this->id       = $array['id'];
        $this->name     = $array['name'];
        $this->email    = $array['email'];
    }
}
