<?php
namespace get_user\V1\Rest\Get_user;

class Get_userResourceFactory
{
    public function __invoke($services)
    {
        return new Get_userResource();
    }
}
