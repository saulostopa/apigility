<?php
/**
 * Created by PhpStorm.
 * User: saulostopa
 * Date: 2/26/17
 * Time: 12:24 PM
 */

namespace get_user\V1\Rest\Get_user;
use Zend\Db\TableGateway\TableGateway;

class Get_userMapper {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}