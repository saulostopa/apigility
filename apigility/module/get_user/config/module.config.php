<?php
return [
    'service_manager' => [
        'factories' => [
            \get_user\V1\Rest\Get_user\Get_userResource::class => \get_user\V1\Rest\Get_user\Get_userResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'get_user.rest.get_user' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/get_user[/:get_user_id]',
                    'defaults' => [
                        'controller' => 'get_user\\V1\\Rest\\Get_user\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'get_user.rest.get_user',
        ],
    ],
    'zf-rest' => [
        'get_user\\V1\\Rest\\Get_user\\Controller' => [
            'listener' => \get_user\V1\Rest\Get_user\Get_userResource::class,
            'route_name' => 'get_user.rest.get_user',
            'route_identifier_name' => 'get_user_id',
            'collection_name' => 'get_user',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \get_user\V1\Rest\Get_user\Get_userEntity::class,
            'collection_class' => \get_user\V1\Rest\Get_user\Get_userCollection::class,
            'service_name' => 'get_user',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'get_user\\V1\\Rest\\Get_user\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'get_user\\V1\\Rest\\Get_user\\Controller' => [
                0 => 'application/vnd.get_user.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'get_user\\V1\\Rest\\Get_user\\Controller' => [
                0 => 'application/vnd.get_user.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \get_user\V1\Rest\Get_user\Get_userEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'get_user.rest.get_user',
                'route_identifier_name' => 'get_user_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \get_user\V1\Rest\Get_user\Get_userCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'get_user.rest.get_user',
                'route_identifier_name' => 'get_user_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'get_user\\V1\\Rest\\Get_user\\Controller' => [
            'input_filter' => 'get_user\\V1\\Rest\\Get_user\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'get_user\\V1\\Rest\\Get_user\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'username',
                'field_type' => 'string',
                'error_message' => 'Data not found',
            ],
        ],
    ],
];
