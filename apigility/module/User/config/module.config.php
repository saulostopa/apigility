<?php
return [
    'controllers' => [
        'factories' => [],
    ],
    'router' => [
        'routes' => [],
    ],
    'zf-versioning' => [
        'uri' => [],
    ],
    'zf-rpc' => [],
    'zf-content-negotiation' => [
        'controllers' => [],
        'accept_whitelist' => [],
        'content_type_whitelist' => [],
    ],
    'zf-content-validation' => [],
    'input_filter_specs' => [
        'User\\V1\\Rpc\\GetUser\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'description' => 'If of user',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'name',
                'description' => 'Name of user',
                'field_type' => 'string',
            ],
        ],
    ],
];
