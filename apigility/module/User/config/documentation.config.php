<?php
return [
    'User\\V1\\Rpc\\GetUser\\Controller' => [
        'description' => 'User',
        'GET' => [
            'description' => 'Get user by id',
            'response' => '{
   "id": "If of user",
   "name": "Name of user"
}',
        ],
    ],
];
