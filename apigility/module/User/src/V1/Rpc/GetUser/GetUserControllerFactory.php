<?php
namespace User\V1\Rpc\GetUser;

class GetUserControllerFactory
{
    public function __invoke($controllers)
    {
        return new GetUserController();
    }
}
