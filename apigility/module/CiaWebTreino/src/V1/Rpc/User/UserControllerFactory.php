<?php
namespace CiaWebTreino\V1\Rpc\User;

class UserControllerFactory
{
    public function __invoke($controllers)
    {
        return new UserController();
    }
}
