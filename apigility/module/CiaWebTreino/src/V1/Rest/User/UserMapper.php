<?php
/**
 * Created by PhpStorm.
 * User: saulostopa
 * Date: 2/26/17
 * Time: 7:07 PM
 */

namespace CiaWebTreino\V1\Rest\User;
use Zend\Db\TableGateway\TableGateway;

class UserMapper {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}