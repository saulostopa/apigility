<?php
namespace CiaWebTreino;

use CiaWebTreino\V1\Rest\User\UserEntity;
use CiaWebTreino\V1\Rest\User\UserMapper;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'UserTableGateway' => function($ServiceManager) {
                    $dbAdapter = $ServiceManager->get('ciawebtreino');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserEntity());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                'CiaWebTreino\V1\Rest\User\UserMapper' => function($ServiceManager) {
                    $tableGateway = $ServiceManager->get('UserTableGateway');
                    return new UserMapper($tableGateway);
                }
            )
        );
    }

    public function getAutoloaderConfig()
    {
        return [
            'ZF\Apigility\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src',
                ],
            ],
        ];
    }
}
