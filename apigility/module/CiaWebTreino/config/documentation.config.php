<?php
return [
    'CiaWebTreino\\V1\\Rpc\\User\\Controller' => [
        'description' => 'User',
        'GET' => [
            'description' => 'List users',
            'response' => '{
    "id": "id of user",
    "first_name": "First name",
    "last_name": "Last name",
    "email": "E-mail",
    "username": "UserName"
}',
        ],
    ],
];
