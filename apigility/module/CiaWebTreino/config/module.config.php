<?php
return [
    'controllers' => [
        'factories' => [],
    ],
    'router' => [
        'routes' => [
            'cia-web-treino.rest.user' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/user[/:user_id]',
                    'defaults' => [
                        'controller' => 'CiaWebTreino\\V1\\Rest\\User\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'cia-web-treino.rest.user',
        ],
    ],
    'zf-rpc' => [],
    'zf-content-negotiation' => [
        'controllers' => [
            'CiaWebTreino\\V1\\Rest\\User\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'CiaWebTreino\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.cia-web-treino.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'CiaWebTreino\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.cia-web-treino.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-content-validation' => [
        'CiaWebTreino\\V1\\Rest\\User\\Controller' => [
            'input_filter' => 'CiaWebTreino\\V1\\Rest\\User\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'CiaWebTreino\\V1\\Rpc\\User\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'description' => 'id',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'first_name',
                'field_type' => 'string',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'last_name',
                'field_type' => 'string',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\EmailAddress::class,
                        'options' => [
                            'useDomainCheck' => true,
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'email',
                'field_type' => 'string',
            ],
            4 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'username',
                'field_type' => 'string',
            ],
        ],
        'CiaWebTreino\\V1\\Rest\\User\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'id',
                'description' => 'id',
                'field_type' => 'integer',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'email',
                'field_type' => 'string',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'username',
                'field_type' => 'string',
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            \CiaWebTreino\V1\Rest\User\UserResource::class => \CiaWebTreino\V1\Rest\User\UserResourceFactory::class,
        ],
    ],
    'zf-rest' => [
        'CiaWebTreino\\V1\\Rest\\User\\Controller' => [
            'listener' => \CiaWebTreino\V1\Rest\User\UserResource::class,
            'route_name' => 'cia-web-treino.rest.user',
            'route_identifier_name' => 'user_id',
            'collection_name' => 'user',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \CiaWebTreino\V1\Rest\User\UserEntity::class,
            'collection_class' => \CiaWebTreino\V1\Rest\User\UserCollection::class,
            'service_name' => 'User',
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \CiaWebTreino\V1\Rest\User\UserEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'cia-web-treino.rest.user',
                'route_identifier_name' => 'user_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \CiaWebTreino\V1\Rest\User\UserCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'cia-web-treino.rest.user',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'CiaWebTreino\\V1\\Rest\\User\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
        ],
    ],
];
